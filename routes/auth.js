const express = require('express');
const { check, body } = require('express-validator');

const authController = require('../controllers/auth');
const User = require('../models/user');

const router = express.Router();

router.get('/', authController.getLogin);
router.post(
    '/login',
    [
        body('email')
            .isEmail()
            .withMessage('Please enter a valid email address.')
            .normalizeEmail(),
        body('password', 'Password has to be valid.')
            .isLength({ min: 5 })
            .isAlphanumeric()
            .trim()
    ],
    authController.postLogin
);
router.get('/signup', authController.getSignup);
router.post('/signup',
    [
        check('signupEmail')
            .isEmail()
            .withMessage('Please enter a valid email.')
            .custom((value, { req }) => {
                return User.findOne({ email: value })
                    .then(userDoc => {
                        if (userDoc) {
                            return Promise.reject('E-Mail exists already, please pick a different one.');
                        }
                    })
                    .catch(err => {
                        throw Error(err);
                    });
            })
            .normalizeEmail(),
        body('username', 'Please provide a username.')
            .trim()
            .isLength({ min: 1 }),
        body('signupPassword', 'Please enter a password with only numbers and text and atleast 5 characters.')
            .isLength({ min: 5 })
            .isAlphanumeric()        // Handle no special character(s)
            .trim(),
        body('confirmPassword')
            .trim()
            .custom((value, { req }) => {
                if (value !== req.body.signupPassword) {
                    throw new Error('Password have to match!')
                }
                return true;
            })
    ],
    authController.postSignup
);
router.post('/logout', authController.postLogout);
module.exports = router;