const express = require('express');

const adminController = require('../controllers/admin');
const isAuth = require('../middleware/is-auth');

const router = express.Router();

// /admin/add-product => GET
router.get('/add-details', isAuth, adminController.getAddProduct);

// /admin/dashboard => GET
router.get('/dashboard', isAuth, adminController.getDetails);

// /admin/add-product => POST
router.post('/add-details', isAuth, adminController.postAddDetails);

// /admin/about-us => GET
router.get('/about-us', isAuth, adminController.getAboutUs);

// /admin/teams => GET
router.get('/teams', isAuth, adminController.getTeams);

module.exports = router;