const crypto = require('crypto');   // To create unique secure random value
const bcrypt = require('bcryptjs');
const { validationResult } = require('express-validator');

const User = require('../models/user');

exports.getLogin = (req, res, next) => {
    let message = req.flash('error');
    if (message.length > 0) {
        message = message[0]
    } else {
        message = null;
    }
    res.render('auth/login', {
        pageTitle: 'Login',
        path: '/login',
        errorMessage: message,
        oldInput: {
            email: '',
            password: ''
        },
        validationErrors: []
    });
};

exports.postLogin = (req, res, next) => {
    const email = req.body.email;
    const password = req.body.password;
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return res.status(422).render('auth/login', {
            pageTitle: 'Login',
            path: '/login',
            errorMessage: errors.array()[0].msg,
            oldInput: {
                email: email,
                password: password
            },
            validationErrors: errors.array()
        });
    }

    User.findOne({ email: email })
        .then(user => {
            if (!user) {
                return res.status(422).render('auth/login', {
                    pageTitle: 'Login',
                    path: '/login',
                    errorMessage: 'Invalid email or password.',
                    oldInput: {
                        email: email,
                        password: password
                    },
                    validationErrors: []
                });
            }
            bcrypt.compare(password, user.password)
                .then(passwordDoMatch => {
                    if (passwordDoMatch) {
                        req.session.isLoggedIn = true;
                        req.session.user = user;
                        return req.session.save(err => {
                            console.log(err);
                            res.redirect('/admin/add-details');
                        });
                    }
                    return res.status(422).render('auth/login', {
                        pageTitle: 'Login',
                        path: '/login',
                        errorMessage: 'Invalid email or password.',
                        oldInput: {
                            email: email,
                            password: password
                        },
                        validationErrors: []
                    });
                })
                .catch(err => {
                    console.log(err);
                    res.redirect('/');
                });
        })
        .catch(err => console.log(err));
};

exports.getSignup = (req, res, next) => {
    let message = req.flash('error');
    if (message.length > 0) {
        message = message[0]
    } else {
        message = null;
    }
    res.render('auth/signup', {
        pageTitle: 'Sign Up',
        path: '/signup',
        errorMessage: message,
        oldInput: { 
            signupEmail: '', 
            signupPassword: '', 
            confirmPassword: '' 
        },
        validationErrors: []
    });
};

exports.postSignup = (req, res, next) => {
    const username = req.body.username;
    const signupEmail = req.body.signupEmail;
    const signupPassword = req.body.signupPassword;
    const confirmPassword = req.body.confirmPassword;
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        console.log(errors.array());
        return res.status(422).render('auth/signup', {
            pageTitle: 'Sign Up',
            path: '/signup',
            errorMessage: errors.array()[0].msg,
            oldInput: { 
                username: username,
                signupEmail: signupEmail, 
                signupPassword: signupPassword, 
                confirmPassword: confirmPassword 
            },
            validationErrors: errors.array()
        });
    }

    if (!username || !signupEmail || !signupPassword || !confirmPassword) {
        req.flash('error', 'Please fill all the fields provided.');
        return res.redirect('/signup');
    } else {
        bcrypt
            .hash(signupPassword, 12)
            .then(hashPassword => {
                if (hashPassword) {
                    const user = new User({
                        username: username,
                        email: signupEmail,
                        password: hashPassword,
                        cart: { items: [] }
                    });
                    return user.save();
                }
            })
            .then(savedUser => {
                if (savedUser) {
                    res.redirect('/');
                }
            })
            .catch(err => console.log(err));
    }
};

exports.postLogout = (req, res, next) => {
    req.session.destroy((err) => {
        console.log(err);
        res.redirect('/');
    })
};
