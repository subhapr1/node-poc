const Details = require('../models/details');

exports.getAddProduct = (req, res, next) => {
    res.render('admin/add-details', {
        pageTitle: 'Add Product',
        path: '/admin/add-details',
        username: req.user.username,
        lastLoginDate: `${new Date().getDate()}/${new Date().getMonth() + 1}/${new Date().getFullYear()}`,
        editing: false
    });
};

exports.postAddDetails = (req, res, next) => {
    const success = true;
    const userId = req.user._id;
    const name = req.body.name;
    const last_login = req.body.last_login;
    const balance = req.body.balance;
    const date = req.body.date;
    const amount = req.body.amount;
    const description = req.body.description;

    const userDetails = new Details({
        success: success,
        customer: {
            userId: userId,
            name: name,
            last_login: last_login.toString(),
            balance: balance,
            transaction: [{
                date: date.toString(),
                amount: amount,
                description: description
            }]
        }
    });
    userDetails.save()
        .then(result => {
            console.log('User Details Created!');
            res.redirect(`/admin/dashboard`);
        })
        .catch(err => console.log(err));
};

exports.getDetails = (req, res, next) => {
    Details
        .find()
        .then(details => {
            let lastLoginIndex = details.length - 1;
            const userTransaction = details.map(item => item.customer.transaction), finalTransactions = [];
            let detailsIndex = details.length - 1;
            for (let i = 0; i < userTransaction.length; i++) {
                for (let j = 0; j < userTransaction[i].length; j++) {
                    finalTransactions.push(userTransaction[i][j]);
                }
            }
            res.render('admin/dashboard', {
                userName: details[0].customer.name,
                lastLogin: details[lastLoginIndex].customer.last_login,
                accbalance: details[detailsIndex].customer.balance,
                transaction: finalTransactions,
                pageTitle: 'User Details',
                path: '/admin/dashboard'
            });
        })
        .catch(err => console.log(err));
};

exports.getAboutUs = (req, res, next) => {
    res.render('user/about-us', {
        pageTitle: 'About Us',
        path: '/about-us'
    });
};

exports.getTeams = (req, res, next) => {
    res.render('user/teams', {
        pageTitle: 'Teams',
        path: '/teams',
        teamMembers: ['Beng Tiong Tang', 'Niraj Kumar Jha', 'Siddharth Pandey', 'Jatin Suri',
            'Parasmani Jain', 'Saurabh Nilegaonkar', 'Bhawana Sharma', 'Vimlesh Singh']
    });
};