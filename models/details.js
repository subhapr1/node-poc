const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const detailsSchema = new Schema({
    success: {
        type: Boolean,
        required: true
    },
    customer: {
        userId: {
            type: Schema.Types.ObjectId,
            required: true,
            ref: 'User'
        },
        name: {
            type: String,
            required: true
        },
        last_login: {
            type: String,
            required: true
        },
        balance: {
            type: Number,
            required: true
        },
        transaction: [{
            date: {
                type: String,
                required: true
            },
            amount: {
                type: Number,
                required: true
            },
            description: {
                type: String,
                required: true
            }
        }]
    }
});

module.exports = mongoose.model('Details', detailsSchema);