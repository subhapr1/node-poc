# iJavascript

iJavascript is a web application which allows user to add transaction details, view details etc.
It has screens / features like below:
1. Sign up
- Validation has been implemented for all the fields such as email should be of correct format, 
- password should be of min. 5 characters and can be alpha numeric **(Eg. 12345a)**
- Mandatory field validations etc. 
2. Sign in
3. Add user details
4. View User details
5. About us
6. Teams screen
7. Logout

Note: User must sign up first to login, and then needs to add user transaction details to view the dashboard table data.
